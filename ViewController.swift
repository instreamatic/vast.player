//
//  ViewController.swift
//  InstreamaticSDK
//
//  Created by Roman on 5/13/21.
//

import AVFoundation
import AVKit
import UIKit

class ViewController: UIViewController {
    
    // MARK: Content
    static let ContentURLString = "https://test.instreamatic.com/static/dialog_example.xml"

        var playerViewController: AVPlayerViewController!

        override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black;
        setUpContentPlayer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
//            self.playAd()
//        }
        
        self.playAd()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(30)) {
//            self.pauseAd()
//        }
    }

    // MARK: Player
    func setUpContentPlayer() {
        let contentURL = URL(string: ViewController.ContentURLString)!
        let player = AVPlayer(url: contentURL)
        playerViewController = AVPlayerViewController()
        playerViewController.player = player

        showContentPlayer()
    }

    func showContentPlayer() {
        self.addChild(playerViewController)
        playerViewController.view.frame = self.view.bounds
        self.view.insertSubview(playerViewController.view, at: 0)
        playerViewController.didMove(toParent:self)
    }

    func hideContentPlayer() {
        playerViewController.willMove(toParent:nil)
        playerViewController.view.removeFromSuperview()
        playerViewController.removeFromParent()
    }
    
    
    
    
    // MARK: Control
    func pauseAd() {
        playerViewController.player?.pause()
        hideContentPlayer()
    }

    func playAd() {
        showContentPlayer()
        playerViewController.player?.play()
    }
    
    
    
}
      

